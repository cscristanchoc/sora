from django import forms

municipalities = [('Funza', 'Funza'), ('Soacha', 'Soacha'),
                  ('Mosquera', 'Mosquera'), ('Bojacá', 'Bojacá'),('Santafe de Bogotá', 'Santafe de Bogotá')]
years = [
    ('2019', '2019'),
    ('2018', '2018'),
    ('2017', '2017'),
    ('2016', '2016'),
    ('2015', '2015'),
    ('2014', '2014'),
    ('2013', '2013')]


class MunicipalityForm(forms.Form):
    municipality = forms.CharField(label="Municipio", required=True,
                                   widget=forms.Select(
                                       attrs={'class': 'selectpicker', 'data-live-search':'true',
                                              'title': 'Municipio no seleccionado'},
                                       choices=municipalities)
                                   )
    year = forms.CharField(label="Año", required=True,
                            widget=forms.Select(
                                attrs={'class': 'selectpicker', 'data-live-search': 'true',
                                       'title': 'Año no seleccionado'},
                                choices=years))

