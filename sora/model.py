import pandas as pd
import os

precipitations = ['1', '1.01', '0.02', '0.03', '1.04', '1.05', '3.06', '1.07', '1.08', '1.09', '1.1', '1.11']
dirname = os.path.dirname(__file__)

df = pd.read_csv(
    dirname + "/Precipitaciones_Totales_Mensuales.csv")  # Importar el dataset df.columns                              #Conocer las columnas
df.head()  # Ver los primeros resultados
variables = ['ESTACIÓN', 'CÓDIGO', 'MUNICIPIO', 'AÑO', 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO',
             'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
df = df[variables]
print(df.duplicated().sum())  # Duplicados del DataFrame
print(df.duplicated(subset=['CÓDIGO', 'AÑO']).sum())  # Duplicados de la serie
arreglo = df.duplicated(subset=['CÓDIGO', 'AÑO'])

for idx, val in enumerate(arreglo):
    if (val == True):
        df['CÓDIGO'][idx] = df['CÓDIGO'][idx] + 10000

df.duplicated().sum()
arreglo2 = df.isnull()
for index, row in arreglo2.iterrows():
    for i in range(len(row)):
        if row[i] == 1:
            if row[i].dtype == str:
                row[i] = "VACIO"
            elif row[i].dtype == int:
                row[i] = 0
            else:
                row[i] = 0


def data(municipality, year):
    df.groupby(['AÑO'])
    escala = df.sort_values('AÑO', ascending=True)
    print(escala)
    b = str(municipality)
    FILTER1 = df['MUNICIPIO'] == b
    filtered2 = df[FILTER1]
    a = int(year)
    FILTER1 = filtered2['AÑO'] == a
    filtered2 = filtered2[FILTER1]
    # df.to_csv('precipitaciones_limpias.csv')
    if (len(filtered2) > 0):
        return [filtered2.iloc[0]['ENERO'], filtered2.iloc[0]['FEBRERO'], filtered2.iloc[0]['MARZO'],
                filtered2.iloc[0]['ABRIL'], filtered2.iloc[0]['MAYO'], filtered2.iloc[0]['JUNIO'],
                filtered2.iloc[0]['JULIO'], filtered2.iloc[0]['AGOSTO'], filtered2.iloc[0]['SEPTIEMBRE'],
                filtered2.iloc[0]['OCTUBRE'], filtered2.iloc[0]['NOVIEMBRE'], filtered2.iloc[0]['DICIEMBRE']]
    return []